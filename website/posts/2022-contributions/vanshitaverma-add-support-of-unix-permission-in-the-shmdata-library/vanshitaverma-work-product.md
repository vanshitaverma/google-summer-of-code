---
title: Work Product template
slug: work-product-vanshitaverma-add-support-of-unix-permission-in-the-shmdata-library
author: Vanshita Verma
date: 2022-09-09 13:45:11 GMT+5:30
tags: contributions, products, shmdata, gstreamer, unix permissions, c/c++
type: text
---

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resource:
https://google.github.io/gsocguides/student/evaluations#final-evaluations-and-work-product-submission
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with work-product-, then your your gitlab username and contribution title; the whole string identical to the filename)
* author (your name as contributor)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: contributions, products; tools from SAT) 
-->

## Name and Contact Information

<!-- Please your public contact information below this comment and add your name as value in post author metadata -->

* name: Vanshita Verma
* gitlab username: vanshitaverma
* timezone: GMT+5:30

<!-- Please do not share private contact information yet (email addresses, telephone number). -->

## Title

<!-- 
Please use the same title as for your proposal
-->

Add Support of Unix Permission in the Shmdata Library

## Short description of work done

<!-- 
Please write a short description of work done. 
(150-200 words)
-->

The project was aimed at adding UNIX permissions to the shmdata library. Essentially, the work comprised checking and providing access to the shared data streams according to the permissions given while writing/sending the data stream. Changes were made to the files of code that dealt with the UNIX socket server, semaphores, and shared memory.

The code files were changed from getting default read and write permissions to have a bit-wise octal mode variable so that we could set the permissions according to requirement.

The work done mostly revolved around creating a test that checks the UNIX permissions of the data stream file. A test file was coded/written to check the read, write and execute permissions of the data file we are dealing with. It also checks if the owner, group or user is trying to access the data file, if it has permissions to perform the function, and reports the status of the file.

## What code got merged

<!-- 
Please list all merge requests that got merged from all repositories related to your GSoC contribution (except: https://gitlab.com/sat-mtl/google-summer-of-code)
-->

Merge Request along with the commits made throughout the course of the program:

Link to the MR: https://gitlab.com/sat-mtl/tools/shmdata/-/merge_requests/71 

Adding Unix Permissions setting to the CPP and C writer


## What code didn’t get merged

<!-- 
Please list all merge requests that did not get merged from all repositories related to your GSoC contribution (except: https://gitlab.com/sat-mtl/google-summer-of-code)
-->

Commits in the merge request that were later removed because they needn't be merged into the repository as new development was made midway through the project:

* unix-perms interface file (shmdata/unix-perms.hpp) (https://gitlab.com/sat-mtl/tools/shmdata/-/commit/c582cfce3fc6dd5d08eb055c81ff7ad5f353d824)

* unix-perms implementation file (shmdata/unix-perms.cpp) (https://gitlab.com/sat-mtl/tools/shmdata/-/commit/1f6c0f5b9de223d8720eff9c64909410794d68df)

## What’s left to do

<!-- 
Please write a short description of future work left to do. 
(150-200 words)
-->

All proposed features before the start of the GSoC program implemented.
