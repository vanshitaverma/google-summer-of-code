---
title: Add Support of UNIX Permission in the Shmdata Library - GSoC'22 Program Conclusion
slug: 2022-09-09-blog
author: Vanshita Verma
date: 2022-09-09 11:01:11 GMT+5:30
tags: contributions, blog, shmdata, unix permissions
type: text
---

# Title

Hello there, my name is Vanshita Verma and I'm writing this blog to enclose my experience as a Google Summer of Code 2022 contributor for the organisation the Society of Arts and Technology. I was an open-source software development intern for the research and development department of SAT Metalab.

My journey with SAT began way back in June during the community bonding period of the GSoC'22 program. We had meetings to discuss the work to be done during the next few months and how to start with the coding process.

My project through the duration of the program revolved around adding UNIX permissions to the Shmdata library. The Shmdata library provides a layer to share streams of framed data between processes via shared memory. I was tasked to add the compatibility for data access permissions. The code had to check whether the given data stream file had the necessary permissions to read, write or execute the file. It also checked whether the owner, group or user is trying to access the data file and consequently displayed if they had permission to perform the task.

It took me a long while to familiarize myself with the repository and the code of the Shmdata library and I was grateful to have mentors who guided me at every step of the process.

When I started with my contributions to the repository, I had assumed that I had to write the code to change the permissions of the data streams but was quickly made aware that my task was to check the permissions and provide access as permitted. 

Many times during the length of the program, I was stuck at places with seemingly insurmountable challenges and problems ahead but got through them slowly and gradually as I spent more time on the code and reading/understanding how it worked.

My mentors, Nicolas Bouillot, Thomas Piquet, and Valentin Laurent were immensely helpful with their suggestions and inputs, all the while being extremely patient in solving my doubts (and many times my errors).

Around the conclusion of the coding period, my mentor, Mr Nicolas Bouillot made changes in my code sample and shifted most of the code from the interface and implementation files to the test file.

At the end of the program, I was elated to know that the merge request I submitted was successfully merged to the repository and will be in the next Shmdata release.