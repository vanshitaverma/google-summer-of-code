---
title: Webcam based avatar facial expression rigging and improved Interaction controls in Mozilla Hubs for Satellite
slug: proposal-DipanshuShukla-webcam-based-avatar-facial-expression-rigging-and-interaction-controls-in-Mozilla-hubs-for-satellite
author: Dipanshu Shukla
date: 2022-04-16 12:00:00 UTC+05:30
tags: proposals, hard, 350 hours, Satellite, LivePose, MediaPipe, metaverse, telepresence
type: text
---

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resource:
https://google.github.io/gsocguides/student/writing-a-proposal
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with proposal-, identical as filename)
* author (your name as contributor)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: proposals, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

<!--
If you feel you must have graphical or interactive content associated with your application, please include this content in your merge request by prefixing the filenames with your page slug then followed by dash-separated words and embed this content with markdown syntax into your proposal. 
We prefer that you do not include of portrait picture of yourself yet, we want to avoid unintentional bias from reviewers. 
-->

<!--
We encourage participation from students from communities under-represented in the Science, Technology, Engineering, Arts, and Mathematics (STEAM) fields. We aim at increasing the participation of groups traditionally underrepresented in software development (including but not limited to: women, LGBTQ+, underrepresented minorities, and people with disabilities).
-->

## Name and Contact Information

<!-- Please your public contact information below this comment and add your name as value in post author metadata -->

* name: Dipanshu Shukla
* website: [https://github.com/DipanshuShukla](https://github.com/DipanshuShukla)
* gitlab username: [@DipanshuShukla](https://gitlab.com/DipanshuShukla)
* timezone: UTC+5:30

<!-- Please do not share private contact information yet (email addresses, telephone number), we will ask you for this information later if your proposal is accepted. -->

## Project title/description

<!-- 
Your title should be short, clear and interesting. 
The job of the title is to convince the reviewer to read your synopsis. 
-->

Webcam based avatar facial expression rigging and improved Interaction controls in Mozilla Hubs for Satellite

## Synopsis

<!-- 
Start your proposal with a short summary, designed to convince the reviewer to read the rest of the proposal. 
(150-200 words)
-->

<!-- https://research.google/pubs/pub50903/ -->


The metaverse is essentially a new immersive version of the internet. It is said to be the next evolution of social connection. Whether in virtual reality (VR), augmented reality (AR) or simply on a screen, the promise of the metaverse is to allow a greater overlap of our digital and physical lives in wealth, socialization, productivity, shopping and entertainment.

Immersed VR has already netted millions in investment dollars and partnered with Facebook, Microsoft and Samsung in various roles. And for companies developing headsets, the COVID-19 work shake up provides an opportunity to do just as Renji Bijoy, Immersed VR’s founder and CEO suggests, making the case that VR is less of a novelty and more of a quality-of-life tool.

In metaverse motion tracking technology is used to track the body position/motion of the user and projected onto their avatars inside the virtual world through a technology generally referred to as rigging. 
This increases the immersion and enhances the experience for the user as well as the others involved. Similarly, the experience can be made much more natural and better if similar technology is applied for facial (expression) rigging.

Traditional methods for facial rigging Motion Capture involve the use of expensive laser scanners or infrared camera setups with IR reflective markers attached onto the face. However, these methods are somewhat tedious and cost a lot.

Following images are the example of tedious and expensive equipment for facial MoCap:

![website/posts/2022-proposals/proposal-DipanshuShukla-webcam-based-avatar-facial-expression-rigging-and-interaction-controls-in-Mozilla-hubs-for-satellite_avatar-movie-facial-tracking.jpeg](website/posts/2022-proposals/proposal-DipanshuShukla-webcam-based-avatar-facial-expression-rigging-and-interaction-controls-in-Mozilla-hubs-for-satellite_avatar-movie-facial-tracking.jpeg)

![website/posts/2022-proposals/proposal-DipanshuShukla-webcam-based-avatar-facial-expression-rigging-and-interaction-controls-in-Mozilla-hubs-for-satellite_CPBP.gif](website/posts/2022-proposals/proposal-DipanshuShukla-webcam-based-avatar-facial-expression-rigging-and-interaction-controls-in-Mozilla-hubs-for-satellite_CPBP.gif)

This GSoC project propose the development of a new low-cost facial expression rigging Motion Capture technique using computer vision and AI through a webcam. Further, this technology will be integrated in the [Mozilla Hubs](https://hubs.mozilla.com/) along with some other controls to improve telepresence interactions.

These days similar type of techniques are used by [vtubers](https://en.wikipedia.org/wiki/VTuber).

Following are some example images of these techniques:

![website/posts/2022-proposals/proposal-DipanshuShukla-webcam-based-avatar-facial-expression-rigging-and-interaction-controls-in-Mozilla-hubs-for-satellite_d8c.gif](website/posts/2022-proposals/proposal-DipanshuShukla-webcam-based-avatar-facial-expression-rigging-and-interaction-controls-in-Mozilla-hubs-for-satellite_d8c.gif)

<p align="center">
<img src="proposal-DipanshuShukla-webcam-based-avatar-facial-expression-rigging-and-interaction-controls-in-Mozilla-hubs-for-satellite_one.gif">
</p>

## Benefits to Community

<!-- 
Make your case for a benefit to the organization, not just to yourself. 
Why would Google and SAT be proud to sponsor this work? 
How would open source or society as a whole benefit? 
What cool things would be demonstrated?
(150-200 words)
-->
The [Satellite](https://hub.satellite.sat.qc.ca/about/) is a project developed by the [Society for Arts and Technology (SAT)](https://sat.qc.ca/en) that is an innovative new virtual campus and hybrid tool offering an interactive environment dedicated to teaching in order to promote the discovery and learning of technological arts. Satellite Hub builds upon [Mozilla Hubs](https://hubs.mozilla.com/), which is a platform based on **Metaverse** (a network of 3D virtual worlds focused on social connection) and provides a more immersive experience for social media users by enticing a feeling of presence among them.

Metaverse enables students to get fully immersed in a digital classroom environment. The students can interact with the instructor and learn content in an interactive environment. All day-to-day activities like classes, workshops, projects, experiments, and seminars happen in real-time, enabling effective student participation.

VR & AR-based education has a huge advantage in delivering theoretical and practical knowledge. It can make abstract problems concrete. It helps sharpen students’ operational skills, provides an immersive learning experience, and improves students’ involvement in class, making learning more fun and active.

This project aims to enhance the user interactive experience of Mozilla Hubs by adding human-like facial expressions to the user's virtual avatar, directly/indirectly mimicking the user's behavior (facial expressions) and making the experience more lively. Doing all this without the need for expensive equipment with just the use of an affordable/accessible webcam.


## Deliverables

<!-- 
Include a brief, clear work breakdown structure with milestones and deadlines. 
Make sure to label deliverables as optional or required. 
You may want your plan to start by producing some kind of technical paper, or planning the project in traditional Software Engineering style. 
It’s OK to include thinking time (“investigation”) in your work schedule. 
Deliverables should include investigation, coding and documentation.
(max 1000 words)
-->

### Goals
* Prototype various face feature tracking approaches
* understanding the predeveloped existing technologies (LivePose, MediaPipe, etc.)
* Development of webcam based facial rigging and controls
* Experience the design space of eye and face pose estimation with LivePose and MediaPipe.
* Integrate eye/face interaction techniques directly in Mozilla Hubs used for Satellite.
* Improving Interaction controls
* Provide relevant Documentation

### Timeline
#### May - June 12
* Community Bonding Period
* Decide on schedule and mode of communication for weekly and emergency meetings.
* Setup a blog for GSoC.
* Understand the codebase
#### June 13 - July 1
* Research (Internet, research papers, video conferences etc) for various approaches/resources
* Create a list of relevant findings for future reference
#### July 2 - July 20

* Trying/prototyping various approaches
* Deciding on a concrete implementation approach
* Documenting the findings
* Start to look into LivePose

#### July 22 - July 24

* Code Freeze
* Buffer Period to work on any backlogs
* code refactoring
* fixing new bugs
#### July 25 - July 29
* Evaluation Phase 1
#### July 30 - August 16
* Read and understand the documentation of LivePose
* start to work on LivePose and MediaPipe.
* Integrate The chosen face feature tracking model with LivePose
#### August 17 - September 2
* Read and understand documentation for mozilla hubs
* research face rigging
* Prototype face rigging 3d face models using livepose and blender
* document the findings
* start writing documentation
#### September 3 - September 7
* Code Freeze
* Buffer Period to work on any backlogs
* code refactoring
* fixing new bugs
#### September 8 - September 29
* start adding face rigging capabilities into mozilla hubs avatars
* Integration testing with LivePose and MediaPipe.
* continue writing documentation
#### September 30 - October 19
* create various other webcam based interaction enhancing controls for the avatar 
* Integration testing
* continue writing documentation
#### October20 - November 20
* Finalizing the code
* Refactoring code
* Squashing Bugs
* Completing the Documentation
#### November 21
* Final date for all contributors to submit their work product
#### November 28
* Final date for mentors to submit evaluations for GSoC contributor projects with extended deadlines 
## Related Work

<!-- 
You should understand and communicate other people’s work that may be related to your own. 
Do your research, and make sure you understand how the project you are proposing fits into the target organization. 
Be sure to explain how the proposed work is different from similar related work.
(max 1000 words)
-->

* [C. Sagonas, E. Antonakos, G, Tzimiropoulos, S. Zafeiriou, M. Pantic. 300 faces In-the-wild challenge: Database and results. Image and Vision Computing (IMAVIS), Special Issue on Facial Landmark Localisation "In-The-Wild". 2016.](https://ibug.doc.ic.ac.uk/media/uploads/documents/sagonas_2016_imavis.pdf)
* [C. Sagonas, G. Tzimiropoulos, S. Zafeiriou, M. Pantic. 300 Faces in-the-Wild Challenge: The first facial landmark localization Challenge. Proceedings of IEEE Int’l Conf. on Computer Vision (ICCV-W), 300 Faces in-the-Wild Challenge (300-W). Sydney, Australia, December 2013.](https://ibug.doc.ic.ac.uk/media/uploads/documents/sagonas_iccv_2013_300_w.pdf)
* [C. Sagonas, G. Tzimiropoulos, S. Zafeiriou, M. Pantic. A semi-automatic methodology for facial landmark annotation. Proceedings of IEEE Int’l Conf. Computer Vision and Pattern Recognition (CVPR-W), 5th Workshop on Analysis and Modeling of Faces and Gestures (AMFG 2013). Oregon, USA, June 2013.](https://ibug.doc.ic.ac.uk/media/uploads/documents/sagonas_cvpr_2013_amfg_w.pdf)
* [jkirsons/FacialMotionCapture_v2 - Blender & OpenCV - Facial Motion Capture v2](https://github.com/jkirsons/FacialMotionCapture_v2)



## Biographical Information

<!-- 
Keep your personal info brief. 
Be sure to communicate personal experiences and skills that might be relevant to the project. 
Summarize your education, work, and open source experience. 
List your skills and give evidence of your qualifications. 
Convince your organization that you can do the work. 
Any published work, successful open source projects and the like should definitely be mentioned.
(max 200 words)
-->

I am currently a final year Computer Science student from India pursuing a bachelor's degree. 
I am proficient in python and have experience developing ML and Dl application projects.

I chose this project because I am passionate about VR and AR and am intrigued by meta verse. Also I am a big fan of vtubing and the technology behind it.

Furthermore, I have also implemented Deep Learning applications from research papers one of which can be found [here](https://github.com/DipanshuShukla/Slider-based-Control-GAN).

## Skills
* Required: experience with JavaScript
    * I have worked on some interesting javascript projects including this one right [here](https://github.com/DipanshuShukla/Filtr). 
* Preferred: experience with Python
    * As mentioned before I am very proficient in python and have developed various applications in python including an implementation of Machine learning [NEAT](https://en.wikipedia.org/wiki/Neuroevolution_of_augmenting_topologies) algorithm in the game of flappy bird [here](https://github.com/DipanshuShukla/NEAT-AI-Plays-Flappy-Bird).
* Preferred: understanding of the fundamentals of deep learning and computer vision
    * I am good with ML and DL including computer vision. As mentioned earlier I have implemented a DL project, from reading the documentation, which can be found [here](https://github.com/DipanshuShukla/Slider-based-Control-GAN).

## Expected size of project 

<!--
Please write below this comment either: 175 hours or 350 hours
Please also adjust accordingly tags in post metadata.
-->


350 hours

## Rating of difficulty 

<!--
Please write below this comment either: easy, medium or hard
Please also adjust accordingly tags in post metadata.
 -->

hard
