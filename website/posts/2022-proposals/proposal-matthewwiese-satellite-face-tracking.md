---
title: Face Tracking to Improve Accessibility and Interaction in the Metaverse with Satellite
slug: proposal-matthewwiese-satellite-face-tracking
author: Matthew Wiese
date: 2022-02-24 12:00:00 UTC-05:00
tags: proposals, hard, 350 hours, Satellite, LivePose, MediaPipe, metaverse, telepresence
type: text
---

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resource:
https://google.github.io/gsocguides/student/writing-a-proposal
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with proposal-, identical as filename)
* author (your name as contributor)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: proposals, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

<!--
If you feel you must have graphical or interactive content associated with your application, please include this content in your merge request by prefixing the filenames with your page slug then followed by dash-separated words and embed this content with markdown syntax into your proposal. 
We prefer that you do not include of portrait picture of yourself yet, we want to avoid unintentional bias from reviewers. 
-->

<!--
We encourage participation from students from communities under-represented in the Science, Technology, Engineering, Arts, and Mathematics (STEAM) fields. We aim at increasing the participation of groups traditionally underrepresented in software development (including but not limited to: women, LGBTQ+, underrepresented minorities, and people with disabilities).
-->

## Name and Contact Information

<!-- Please your public contact information below this comment and add your name as value in post author metadata -->

* name: Matthew Wiese
* website: [https://mattwie.se](https://mattwie.se)
* gitlab username: [@matthewwiese](https://gitlab.com/matthewwiese)
* timezone: UTC-5

<!-- Please do not share private contact information yet (email addresses, telephone number), we will ask you for this information later if your proposal is accepted. -->

## Project title/description

<!-- 
Your title should be short, clear and interesting. 
The job of the title is to convince the reviewer to read your synopsis. 
-->

Face Tracking to Improve Accessibility and Interaction in the Metaverse with Satellite

## Synopsis

<!-- 
Start your proposal with a short summary, designed to convince the reviewer to read the rest of the proposal. 
(150-200 words)
-->

<!-- https://research.google/pubs/pub50903/ -->

The "metaverse" has seen exceptional interest in both tech and the wider market. 
However, metaverse software currently on the market, from [Second Life](https://secondlife.com/) to [Horizon Worlds](https://www.oculus.com/horizon-worlds/), either limit input to traditional input devices like mice and keyboards, or require expensive gear in the form of VR headsets and specialized controllers.
In this project we aim at integrating interaction techniques that rely on human facial and body gestures, similar to these employed in high-fidelity telepresence systems like [Google's Starline](https://blog.google/technology/research/project-starline/), that we will apply to a hybrid presence system and with low-cost hardware to enable accessibility and affordability. 
This GSoC project aims to perform much needed research and development into using face tracking as a viable means to improve the accessibility of 3D virtual spaces, much like those found in the metaverse. 
The result benefits all users, whether or not they have specific needs for accessibility: improved interaction vectors and empathy channels liberate the user to go become human. 
Face tracking software built upon open source technologies will be integrated with [Satellite](https://hub.satellite.sat.qc.ca/about/) / [Mozilla Hubs](https://hubs.mozilla.com/) to provide this crucial feature, enabling a practical testbed for future innovation in hybrid telepresence interaction.
## Benefits to Community

<!-- 
Make your case for a benefit to the organization, not just to yourself. 
Why would Google and SAT be proud to sponsor this work? 
How would open source or society as a whole benefit? 
What cool things would be demonstrated?
(150-200 words)
-->

Accessible technology, especially that used in social technologies like the metaverse, empowers individuals of all kinds to contribute and share in the rich interactions that the system makes possible. Research and development in this area is crucial to allowing the merging of AI/ML and VR/AR into the wider public consciousness. The internet already facilitates incredible human connection - by enabling SAT through Google Summer of Code, we can develop the technologies of the metaverse that enable connection on a truly global scale. From grandchildren to grandparents, we will have developed a model system for natural human gestures and facial expressions to become the natural, empathetic interface to our computers and their link to the constellation of 3D virtual spaces known as the metaverse.

## Deliverables

<!-- 
Include a brief, clear work breakdown structure with milestones and deadlines. 
Make sure to label deliverables as optional or required. 
You may want your plan to start by producing some kind of technical paper, or planning the project in traditional Software Engineering style. 
It’s OK to include thinking time (“investigation”) in your work schedule. 
Deliverables should include investigation, coding and documentation.
(max 1000 words)
-->

### Timeline (approx. monthly)
#### May - June 12<sup>th</sup>
* [Community Bonding Period](http://googlesummerofcode.blogspot.com/2007/04/so-what-is-this-community-bonding-all.html)
* Start contribution document in [GSoC at the SAT repository](https://gitlab.com/sat-mtl/google-summer-of-code)
* Reading & understanding [LivePose](https://gitlab.com/sat-metalab/livepose) / [MediaPipe](https://github.com/google/mediapipe) APIs and [Satellite Hubs Injection Server](https://gitlab.com/sat-mtl/satellite/hubs-injection-server) / [Mozilla Hubs](https://github.com/mozilla/hubs/) integration

#### June 13<sup>th</sup> - July
* Exploratory/investigative work designing interaction scenarios based on insights from literature and ideation
* Experimentation involving additional SDKs/libraries discovered after the beginning of GSoC
* Decision & subsequent writing of implementation plan based upon most successful prototype

#### July - August
* Start work following implementation plan outlined in previous week(s)
* Begin researching potential venues (journals, conferences, etc.) which may be receptive to our topic

#### August - September
* Have developed MVP involing chosen technology ([LivePose](https://gitlab.com/sat-metalab/livepose), [MediaPipe](https://github.com/google/mediapipe), etc.) and Satellite / Mozilla Hubs based upon iterative prototypes
* Perform upstream changes to Satellite involving work originating from MVP
* Select venue and format (paper, presentation, etc.) to disseminate the results of our GSoC project

#### September - October
* Continue refining MVP, until settling a final version which balances technical excellence and project management comprise
* Write/design paper or presentation based upon work during project

#### October - November
* Finalize codebase and documentation, merge all relevant work into Satellite
* Finalize paper/presentation and submit to a variety of outlets suitable to our topic

### Implementation Goals

This is an approximate implementation plan encompassing all major technical priorities, to be completed before the end of July.

* **Explore the design space** offered by eye and face pose estimation with [LivePose](https://gitlab.com/sat-metalab/livepose) and [MediaPipe](https://github.com/google/mediapipe).
* **Integrate eye/face interaction techniques** directly in [Mozilla Hubs](https://hubs.mozilla.com/) used for [Satellite](https://gitlab.com/sat-mtl/satellite/hubs-injection-server).
* **Develop accessible public documentation** involving my work through this GSoC project.
* **Produce audio/video/web content** promoting this GSoC project and Satellite more broadly.

## Related Work

<!-- 
You should understand and communicate other people’s work that may be related to your own. 
Do your research, and make sure you understand how the project you are proposing fits into the target organization. 
Be sure to explain how the proposed work is different from similar related work.
(max 1000 words)
-->

The goal of this project is to contribute to existing open source tools created at the SAT, particularly the [Satellite Hub](https://hub.satellite.sat.qc.ca/about/) project, an immersive 3D social web environment based on the popular [Mozilla Hubs](https://hubs.mozilla.com/).
Satellite seeks to complement the 3D virtual spaces that Mozilla Hubs provides with additional features integral to creating a transdisciplinary platform for promoting various cultural and artistic content, and to create synergy by interconnecting them.

This project builds upon the expertise gained at the SAT throughout the creation of [LivePose](https://gitlab.com/sat-metalab/livepose), a command line tool which tracks people's skeletons from an RGB or grayscale video feed (live or otherwise), and applies various filters on them (for detection, selection, improving the data, etc) and sends the results through the network.
LivePose is able to process video streams and send out results for each frame at 20-30 FPS in real time.
However, LivePose is a Python-based, "backend" software. In the case of Satellite, we require software capable of running client-side in the browser.

Google's [MediaPipe](https://github.com/google/mediapipe) enables live ML processing in the browser.
Specifically, MediaPipe offers solutions for face, hand, and pose tracking.
This allows for a client-side solution which can be integrated with Satellite to provide telepresence accessibility enhancements.

Additionally, there exists a large body of academic research involving the use of face tracking: [Plopski et al. (2022)](https://dl.acm.org/doi/10.1145/3491207) reviewed gaze interaction and eye tracking research related to XR that has been published since 1985. 
[Tuisku et. al (2012)](https://dl.acm.org/doi/10.1016/j.intcom.2011.10.002) engineered a face tracking system, using in part gaze direction, to point and select objects on a screen, which is highly related to the stated goals of this project.
Less recently, [Tu et al. (2005)](https://dl.acm.org/doi/10.1016/j.cviu.2006.11.007) developed a similar system for interpreting a live video feed (*e.g.* from a webcam) as motions which translate into mouse movements.
Although the metaverse is an "old" concept in modern computing (originating in [Neal Stephenson's 1992 novel *Snow Crash*](https://en.wikipedia.org/wiki/Metaverse#Snow_Crash,_1992)), there is a distinct lack of material at the intersection of metaverse/telepresence systems and human-computer interaction research into face tracking. 

This project aims at delivering a end user-focused AI/ML-powered face tracking solution for the metaverse and hybrid telepresence systems, culminating in both a publicly-available enhancement to SAT's Satellite as well as a paper or conference presentation after its conclusion.

## Biographical Information

<!-- 
Keep your personal info brief. 
Be sure to communicate personal experiences and skills that might be relevant to the project. 
Summarize your education, work, and open source experience. 
List your skills and give evidence of your qualifications. 
Convince your organization that you can do the work. 
Any published work, successful open source projects and the like should definitely be mentioned.
(max 200 words)
-->

I am a [Masters student in Data Science](https://www.colorado.edu/program/data-science/), with a development background ranging from bioinformatics to traditional software engineering.

Additionally, I pursued undergraduate research (funded by my University's Arts & Sciences Honors Committee) in electronic literature and computational poetry. This, alongside my bachelors in Philosophy, complements my strong technical skills and underscores my ability to work cross-functionally in the areas of human-computer interaction and the social sciences.

## Skills
* **<u>Required</u>: experience with JavaScript**
    * Past employment as a frontend engineer, with experience in React, jQuery, and "vanilla" JavaScript. Additionally, strong exposure to progress in JavaScript standards since ECMAScript 6 (2015) from arrow functions to the nullish coalescing operator (2020). The frontend of my personal project, [the Find With Frog search engine](https://findwithfrog.com), was developed in React.
* **<u>Preferred</u>: experience with Python**
    * Past experience in an academic setting managing and developing Python data pipelines for bioinformatics; additionally, strong in more traditional software engineering with Python, in particular backends developed with Django, Flask, and FastAPI. The backend of the [Find With Frog search engine](https://findwithfrog.com) is developed in Python using FastAPI and the open source [py-tantivy](https://github.com/quickwit-oss/tantivy-py) package which allows integrating a [Tantivy](https://github.com/quickwit-oss/tantivy)-based search index.
* **<u>Preferred</u>: understanding of the fundamentals of deep learning and computer vision**
    * I am a [Masters student in Data Science](https://www.colorado.edu/program/data-science/) at the University of Colorado Boulder, and so have taken coursework in machine learning and AI. This sets me up well to understand the needs of the project and how to correctly utilize these technologies beyond skin-deep popular knowledge.

## Expected size of project 

<!--
Please write below this comment either: 175 hours or 350 hours
Please also adjust accordingly tags in post metadata.
-->


350 hours

## Rating of difficulty 

<!--
Please write below this comment either: easy, medium or hard
Please also adjust accordingly tags in post metadata.
 -->

hard
