<!--
.. title: Guidelines: from ideas to proposals
.. slug: from-ideas-to-proposals
.. author: Christian Frisson
.. date: 2022-03-16 16:43:11 UTC-04:00
.. tags: guidelines
.. type: text
-->

These guidelines support the process of guiding Mentors on how to help potential Contributors to transition from ideas applications through gitlab issues, to proposals applications through merge requests, towards leading Contributors to an effective application. 

# Background

These guidelines extend recommendations from official GSoC resources, particularly the [Contributor](https://google.github.io/gsocguides/student/) and [Mentor](https://google.github.io/gsocguides/mentor/) guides:

- Contributor guide > Appplying > from [Making First Contact](https://google.github.io/gsocguides/student/making-first-contact) to [Writing a proposal](https://google.github.io/gsocguides/student/writing-a-proposal)
- Mentor guide > Getting Started > [Selecting a student](https://google.github.io/gsocguides/mentor/selecting-a-student)
- Mentor guide > Mentoring > [Workflow](https://google.github.io/gsocguides/mentor/workflow)
- Mentor guide > Org Admin > [Selecting Contributors and Mentors](https://google.github.io/gsocguides/mentor/selecting-students-and-Mentors)

# Terms

* **Project ideas** are drafted by Organization Admins and/or Mentors., their goal is to contextualize potential GSoC contributions.
* **Application ideas** are submitted by potential Contributors through gitlab issues, they are the first step of the application process for potential Contributors, their goal is to review the fitness of the match between the Contributor and the Organization.
* **Proposals** are submitted by potential Contributors through gitlab merge request, they are the second step of the application process for potential Contributors, their goal is to prepare for the formal application of Contributors on the official GSoC portal. 

# Workflow

These guidelines cover 2 phases:

1. Review ideas applications through gitlab issues
2. Review proposals through merge requests to lead to official applications

## Review ideas applications through gitlab issues

Initial condition: 

* a potential GSoC Contributor has opened an issue to discuss an application idea following our [call for proposals](/posts/2022-share/).

The potential GSoC Contributor has:

* signed [up](https://gitlab.com/users/sign_up)/[in](https://gitlab.com/users/sign_in) on [gitlab.com](https://gitlab.com)
* created an issue on [https://gitlab.com/sat-mtl/google-summer-of-code/-/issues](https://gitlab.com/sat-mtl/google-summer-of-code/-/issues) where they have checked tasks in the issue description confirming that they:
    * are eligible to participate as student Contributor by reading section `7. GSoC Contributors` of GSoC rules: [https://summerofcode.withgoogle.com/rules](https://summerofcode.withgoogle.com/rules)
    * are available during the GSoC timeline: [https://developers.google.com/open-source/gsoc/timeline](https://developers.google.com/open-source/gsoc/timeline)
    * have read the GSOC Contributor guide: [https://google.github.io/gsocguides/student/](https://google.github.io/gsocguides/student/)
    * have browsed our project ideas at SAT: [https://sat-mtl.gitlab.io/google-summer-of-code/categories/ideas/](https://sat-mtl.gitlab.io/google-summer-of-code/categories/ideas/)
    * have read our proposals guide at SAT: [https://sat-mtl.gitlab.io/google-summer-of-code/posts/2022-proposals/proposal-welcome/](https://sat-mtl.gitlab.io/google-summer-of-code/posts/2022-proposals/proposal-welcome/)

### Does the application idea mention an existing project idea?

#### Yes, the application idea mentions an existing project idea

One co-author of the project idea or person listed as a potential Co-Mentor should take the initiative to take the lead to:

* thank the potential GSoC Contributor for their application
* review the fitness of items in the gitlab issue description
* help the potential Contributor to proceed to the next step

If no Mentor volunteers within 24 hours, Organization Admins will assign one, from potential Co-Mentors mentioned in the project idea, so that first contact with the potential Contributors is initiated ideally within 36 hours.

#### No, the application idea does not mention an existing project idea

One Organization Admin should take the initiative to take the lead to:

* review the fitness of items in the gitlab issue description
* decide whether the proposed application idea fits within the scope of the Organization:
     * validate that the application idea covers one or more SAT tool(s)

##### Does the proposed application idea fit within the scope of the Organization?

###### Yes, the proposed application idea fits within the scope of the Organization

The Organization Admin should then:

* assign one Mentor to create a project idea related to the application idea of potential Contributor

The assigned Mentor will then:

* thank the potential GSoC Contributor for their application
* help the potential Contributor to proceed to the next step

###### No, the proposed application idea does not fit within the scope of the Organization

The Organization Admin should then:

* thank the potential GSoC Contributor for their application
* inform the potential Contributor that their application idea does not fit within the scope of the Organization
* invite the potential Contributor to choose one existing project ideas, or reorient their application idea, or drop their application

## Review proposals through merge requests to lead to official applications

Mentors will then work together with you Contributors so that they can submit their proposal:

1. first as a [merge request to our repository](https://gitlab.com/sat-mtl/google-summer-of-code/-/merge_requests),
2. then by applying on [https://summerofcode.withgoogle.com](https://summerofcode.withgoogle.com) as required by the GSoC program.

### Review proposals through merge requests

Initial conditions: 

* a potential GSoC Contributor has opened an issue to discuss an application idea following our [call for proposals](/posts/2022-share/).
* at least one Mentor has been assigned to guide the Contributor. 
* a potential GSoC Contributor has opened a merge request to discuss a proposal idea following our [proposal template](/posts/2022-proposals/proposal-welcome/).

The Mentor(s) assigned during the previous phase of application ideation should assign themselves and other potential co-mentors as reviewers of the merge requests.

The Mentor(s) assigned during the previous phase of application ideation should verify that the potential GSoC Contributor has:

* forked our [proposal template](/posts/2022-proposals/proposal-welcome/)
* updated post metadata
* followed all the instructions formatted as inline HTML comments in the markdown source file

The Mentor(s) should then help the Contributor to ready their proposal by:

* [suggesting specific changes](https://docs.gitlab.com/ee/user/project/merge_requests/reviews/suggestions.html) directly where applicable on the markdown source code file, 
* adding general recommendations in the comments thread of the merge request,

Once the Mentor(s) decide(s) that the Contributor has reached a first satisfactory draft of their proposal through their first merge request, the Mentor(s) should:

* approve the first merge request of the Contributor, 
* ensure that this first merge request gets merged by the Contributor, 
* verify the PDF file automatically generated by our gitlab CI configuration among artifacts from the latest pipeline that has passed on the main branch after the merge, 
* invite the Contributor to become member of https://gitlab.com/sat-mtl/google-summer-of-code/ with the Developer role so that the Contributor can open new merge requests from branches in our repository with CI pipelines enabled, 
* consider opening new merge requests (initiated either by the Mentor(s) or by the Contributor) if necessary to revise their proposal.

### Lead Contributors to their effective application on GSoC portal

From April 4, 2022, once official applications opened, we could access new application forms, and we have simulated the workflow of registering as a GSoC contributor and submitting a proposal, so that:

* we better understand the requirements of this process as a GSoC organization,
* we are better equipped to onboard potential GSoC contributors throughout the application process.

This process requires two steps to be completed by contributors, each step with a form to fill:

1. Contributor Registration
2. Proposal Submission

The Mentor(s) should:

* become knowledgeable on the contents of [proposal submission forms](/posts/2022-proposals/proposal-forms/) that contributors should submit,
* verify the consistency of the PDF file automatically generated by our gitlab CI configuration from the markdown file that contributors created during their merge request(s), 
* instruct contributors to ignore figure layout issues,
* remind contributors to upload this PDF file when filling the form their submission, 
* help contributors to successfully submit their proposals by April 19, 2022,
* double-check that their [proposals are in](https://summerofcode.withgoogle.com/organizations/society-for-arts-and-technology-sat/programs/2022/proposals/details).
