<!-- Update YYYY-MM-dd with meeting date and copy to issue title -->
## Weekly Meeting YYYY-MM-dd

### Participants

<!-- 
List usernames of contributor and mentors so that all get notified. 
Check boxes when people are present in meetings (for the record). 
-->

- [ ] @...

### Agenda

<!-- 
Link to issue of previous meeting.
-->

Follows: #...

<!-- 
For each subsection, list SMART tasks (https://en.wikipedia.org/wiki/SMART_criteria) starting by mentioning the username of the person who is responsible with the task. 
For each task, when applicable, link to related issues or merge requests. 
-->

#### Done

- [ ] @... ...

#### Blockers

- [ ] @... ...

#### Todo

- [ ] @... ... 

/label ~weekly-meeting
