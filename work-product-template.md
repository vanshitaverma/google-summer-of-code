---
title: Work Product template
slug: work-product-template
author: Christian Frisson
date: 2022-06-02 13:45:11 UTC-05:00
tags: contributions, products
type: text
---

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resource:
https://google.github.io/gsocguides/student/evaluations#final-evaluations-and-work-product-submission
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with work-product-, then your your gitlab username and contribution title; the whole string identical to the filename)
* author (your name as contributor)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: contributions, products; tools from SAT) 
-->

## Name and Contact Information

<!-- Please your public contact information below this comment and add your name as value in post author metadata -->

* name: ...
* website: ...
* gitlab username: ...
* timezone: ...

<!-- Please do not share private contact information yet (email addresses, telephone number). -->

## Title

<!-- 
Please use the same title as for your proposal
-->

...

## Short description of work done

<!-- 
Please write a short description of work done. 
(150-200 words)
-->

...

## What code got merged

<!-- 
Please list all merge requests that got merged from all repositories related to your GSoC contribution (except: https://gitlab.com/sat-mtl/google-summer-of-code)
-->

* ...

## What code didn’t get merged

<!-- 
Please list all merge requests that did not get merged from all repositories related to your GSoC contribution (except: https://gitlab.com/sat-mtl/google-summer-of-code)
-->

* ...

## What’s left to do

<!-- 
Please write a short description of future work left to do. 
(150-200 words)
-->

...
